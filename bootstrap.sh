#!/bin/bash

apt-get install --force-yes -y software-properties-common python-setuptools

# Enable chris-lea PPA
add-apt-repository -y ppa:chris-lea/node.js

# Refresh the system
apt-get update
apt-get -qy upgrade
apt-get clean

# Install packages
PKGs="curl wget unzip build-essential cmake pkg-config nodejs ruby cython python2.7-dev libfann-dev"
PKGs="$PKGs vsftpd nfs-kernel-server netatalk samba4"

apt-get install --force-yes -qy $PKGs

stop vsftpd ; rm -f /etc/init/vsftpd.conf

gem install --no-rdoc --no-ri foreman

cd /beacon

mkdir -p /srv/{ftp,nfs,afp,samba}
