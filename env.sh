#!/bin/sh

if [[ $SHL_OS == "windows" ]] ; then
    export CYG_PKGs=$CYG_PKGs",ddrescue,dos2unix,e2fsprogs,mkisofs"
fi

#if [[ $SHL_OS == "macosx" ]] ; then
#	ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
#fi

if [[ $SHL_OS == "debian" ]] ; then
    export APT_PKGs=$APT_PKGs" ddrescue dos2unix e2fsprogs mkisofs"
    export APT_PKGs="$APT_PKGs tftpd-hpa nfs-kernel-server vsftpd netatalk samba4"
fi
