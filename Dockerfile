FROM    ubuntu:14.04

# Bundle app source
COPY . /dehliz

# Switch directory
WORKDIR /dehliz

# Install app dependencies
RUN ./bootstrap.sh

VOLUME /srv

EXPOSE  53
EXPOSE  3124
EXPOSE  3142

CMD ["/dehliz/entrypoint.sh"]
